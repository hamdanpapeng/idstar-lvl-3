import java.io.*;
import java.util.*;

public class TrainingLv3 {
    public static void main(String[] args) {

        String DELIMITER = ";";
        String dir = "C://temp/direktori/";

        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dir + "data_sekolah.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(DELIMITER);
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Integer> scores = new ArrayList<>();
        for(List<String> record : records) {
            for(int i = 1; i < record.size(); i++) {
                String score = record.get(i);
                scores.add(Integer.parseInt(score));
            }
        }
        Collections.sort(scores);

        int oneCount = 0;
        int twoCount = 0;
        int threeCount = 0;
        int fourCount = 0;
        int fiveCount = 0;
        int sixCount = 0;
        int sevenCount = 0;
        int eightCount = 0;
        int nineCount = 0;
        int tenCount = 0;

        int sumScore = 0;

        for(Integer score : scores) {
            if(score == 1)
                oneCount++;
            else if(score == 2)
                twoCount++;
            else if(score == 3)
                threeCount++;
            else if(score == 4)
                fourCount++;
            else if(score == 5)
                fiveCount++;
            else if(score == 6)
                sixCount++;
            else if(score == 7)
                sevenCount++;
            else if(score == 8)
                eightCount++;
            else if(score == 9)
                nineCount++;
            else if(score == 10)
                tenCount++;
            sumScore += score;
        }

        double median;
        if (scores.size() % 2 == 0)
            median = ((double)scores.get(scores.size()/2) + (double)scores.get(scores.size()/2 - 1))/2;
        else
            median = (double) scores.get(scores.size()/2);
        double mean = (double)sumScore / (double)scores.size();
        int max = Collections.max(Arrays.asList(oneCount, twoCount, threeCount, fourCount, fiveCount, sixCount, sevenCount, eightCount, nineCount, tenCount));
        int lessThanSixCount = oneCount + twoCount + threeCount + fourCount + fiveCount;
        String modus = "";
        if(max == oneCount)
            modus = "1";
        else if(max == twoCount)
            modus = "2";
        else if(max == threeCount)
            modus = "3";
        else if(max == fourCount)
            modus = "4";
        else if(max == fiveCount)
            modus = "5";
        else if(max == sixCount)
            modus = "6";
        else if(max == sevenCount)
            modus = "7";
        else if(max == eightCount)
            modus = "8";
        else if(max == nineCount)
            modus = "9";
        else if(max == tenCount)
            modus = "10";


        try {
            FileWriter modusFile = new FileWriter(dir+"data_sekolah_modus.txt");
            String modusFileValue = "Berikut Hasil Pengolahan Nilai:\n" +
                    "\n" +
                    "Nilai\t\t\t|\tFrekuensi\n" +
                    "kurang dari 6\t|\t"+lessThanSixCount+"\n" +
                    "6\t\t\t\t|\t"+sixCount+"\n" +
                    "7\t\t\t\t|\t"+sevenCount+"\n" +
                    "8\t\t\t\t|\t"+eightCount+"\n" +
                    "9\t\t\t\t|\t"+nineCount+"\n" +
                    "10\t\t\t\t|\t"+tenCount;
            modusFile.write(modusFileValue);
            modusFile.close();

            FileWriter modusMedianFile = new FileWriter(dir+"data_sekolah_modus_median.txt");
            String modusMedianFileValue = "Berikut Hasil Pengolahan Nilai:\n" +
                    "\n" +
                    "Berikut hasil sebaran data nilai\n" +
                    "Mean: "+mean+"\n" +
                    "Median: "+median+"\n" +
                    "Modus: "+modus;
            modusMedianFile.write(modusMedianFileValue);
            modusMedianFile.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}